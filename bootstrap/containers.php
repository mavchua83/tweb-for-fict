<?php
use Csit\Container\Abc;
use Csit\Container\Auth as AuthContainer;
use Csit\Container\WhoAmI;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;
use SlimSession\Helper as SessionHelper;

$containers = [];

$containers['siteSettings'] = function (ContainerInterface $container) {
    return require __DIR__ . '/settings.php';
};

$containers['whoami'] = function (ContainerInterface $container) {

    return new WhoAmI($container);
};

$containers['abc'] = function (ContainerInterface $container) {
    return new Abc($container);
};

$containers['view'] = function (ContainerInterface $container) {
    return Twig::create(__DIR__ . '/../templates');
};

$containers['session'] = function (ContainerInterface $container) {
    return new SessionHelper();
};

$containers['auth'] = function (ContainerInterface $container) {
    return new AuthContainer($container);
};

return $containers;
?>