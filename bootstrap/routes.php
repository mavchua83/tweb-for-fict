<?php
use Csit\ViewControllers\Home;
use Csit\ViewControllers\Auth;
use Slim\App;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {

    $app->get('/', Home::class . ':getHome')->setName('home');

    $app->get('/abc', Home::class . ':getAbc');

    $app->get('/now', Home::class . ':getNow');

    $app->group('/auth', function(RouteCollectorProxy $group) {

        $group->get('/login', Auth::class . ':getLogin')->setName('auth.login');

        $group->post('/login', Auth::class . ':postLogin');

        $group->get('/logout', Auth::class . ':getLogout')->setName('auth.logout');
    });

    $app->group('/static', function (RouteCollectorProxy $group) {

        $group->get('', function (Request $request, Response $response) {
            $response = $response->withStatus(403);
            return $response;
        })
            ->setName('static');

        $group->get('/vendor', function (Request $request, Response $response) {
            $response = $response->withStatus(403);
            return $response;
        })
            ->setName('staticVendor');
    });
    
};