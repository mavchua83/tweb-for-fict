<?php
$settings = [];

$settings['twig']['template'] = __DIR__ . '/../templates';
$settings['twig']['options']['autoreload'] = true;
$settings['twig']['options']['debug'] = false;
$settings['twig']['options']['cache'] = __DIR__ . '/../compiled/templates';

$settings['container']['compiledPath'] = __DIR__ . '/../compiled/containers';
$settings['container']['proxyPath'] = __DIR__ . '/../compiled/proxies';

$settings['session']['autorefresh'] = true;

$settings['db']['host'] = '127.0.0.1';
$settings['db']['database'] = 'asd';
$settings['db']['username'] = 'lihui';
$settings['db']['password'] = base64_decode('MTIzCg==');

return $settings;