<?php
use DI\ContainerBuilder;
use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Slim\Middleware\Session;

require __DIR__ . '/../vendor/autoload.php';

$settings = require __DIR__ . '/../bootstrap/settings.php';

// Building containers
$containerBuilder = new ContainerBuilder();

$containerBuilder->addDefinitions(require __DIR__ . '/../bootstrap/containers.php');

$containerBuilder->enableCompilation($settings['container']['compiledPath']);

$containerBuilder->writeProxiesToFile(true, $settings['container']['proxyPath']);

$container = $containerBuilder->build();

AppFactory::setContainer($container);

// Create Slim App
$app = AppFactory::create();

$app->addErrorMiddleware(true, true, true);

$app->add(new Session($settings['session']));

$app->add(TwigMiddleware::createFromContainer($app));
// $app->add(new ContentLengthMiddleware());
$routes = require __DIR__ . '/../bootstrap/routes.php';
$routes($app);

// var_dump($app->getRouteResolver());

return $app;
