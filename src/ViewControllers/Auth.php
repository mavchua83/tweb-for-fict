<?php

namespace Csit\ViewControllers;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Auth
{

    private $container = null;

    private $view = null;
    private $auth = null;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('view');

        $this->auth = $this->container->get('auth');

    }

    public function getLogin(Request $request, Response $response)
    {
        $data = [];

        $data['haveValidSession'] = $this->auth->haveValidatedUserSession();
        $data['username'] = $this->auth->getSessionUsername();

        return $this->view->render($response, 'auth/login.html', $data);
    }

    public function postLogin(Request $request, Response $response)
    {
        $data = [];
        $params = $request->getParsedBody();

        $username = trim($params['username'] ?? '');
        

        $data['authenticateResult'] = $this->auth->authenticateUser($username);
        $data['username'] = $this->auth->getSessionUsername();

        return $this->view->render($response, 'auth/login.html', $data);
    }

    public function getLogout(Request $request, Response $response)
    {
        $data = [];

        $this->auth->invalidateUserSession();

        return $this->view->render($response, 'auth/logout.html', $data);
    }
}