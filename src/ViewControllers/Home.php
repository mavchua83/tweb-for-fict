<?php
namespace Csit\ViewControllers;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use DateTime;
use DateTimeZone;

class Home
{

    private $container = null;

    private $view = null;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('view');
    }

    public function getAbc(Request $request, Response $response)
    {
        $data = [];

        $abc = $this->container->get('abc');

        $whoami = $abc->callWhoami();

        $response->getBody()->write($whoami);

        return $response;
    }

    public function getNow(Request $request, Response $response)
    {
        $data = [];

        $datetime = new DateTime('now', new DateTimeZone('Asia/Kuala_Lumpur'));

        $nowString = $datetime->format('Y-m-d H:i:s');

        $data['now'] = $nowString;

        return $this->view->render($response, 'now.html', $data);
    }

    public function getHome(Request $request, Response $response)
    {
        $data = [];

        $abc = $this->container->get('abc');

        return $this->view->render($response, 'home.html', $data);
    }
}