<?php
namespace Csit\Container;

use Psr\Container\ContainerInterface;

class Auth
{

    private $container = null;

    private $db = null;
    private $session = null;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
        //$this->db = $container->get('db');
        $this->session = $container->get('session');
    }

    /**
     * 
     * @return bool true: authenticated, false: failed to authenticate user
     */
    public function authenticateUser(string $username = ''): bool
    {
        $this->invalidateUserSession();

        if(true === empty($username))
        {
            return false;
        }

        $this->session->id(true);

        $this->session->set('session.validSession', true);
        $this->session->set('session.username', $username);

        return true;
    }

    /**
     * 
     * 
     * @return bool true: have, false: no valid user session existed
     */
    public function haveValidatedUserSession(): bool
    {
        return $this->session->get('session.validSession', false);
    }

    /**
     * Invalidate the autnticated user session.
     * 
     * @return void
     */
    public function invalidateUserSession() {
        $this->session->set('session.validSession', false);
        $this->session::destroy();
    }

    public function getSessionUsername(): string
    {
        if(true === $this->haveValidatedUserSession())
        {
            return $this->session->get('session.username', '');
        }
        else
        {
            return '';
        }
    }
}