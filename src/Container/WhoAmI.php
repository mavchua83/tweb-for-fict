<?php
namespace Csit\Container;

class WhoAmI
{

    public function __construct()
    {}

    public function who(): string
    {
        return 'me';
    }
}

?>