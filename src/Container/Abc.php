<?php
namespace Csit\Container;

use Psr\Container\ContainerInterface;

class Abc
{

    private $container = null;

    private $whoami = null;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->whoami = $container->get('whoami');
    }

    public function callWhoami()
    {
        return $this->whoami->who();
    }
}