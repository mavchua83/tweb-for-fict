<?php
date_default_timezone_set('Asia/Kuala_Lumpur');

$app = require __DIR__ . '/../bootstrap/app.php';

$app->run();